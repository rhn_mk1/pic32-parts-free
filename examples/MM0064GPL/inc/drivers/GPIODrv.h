#ifndef GPIODRV_H_ef314d0218d04f29abd02c15d2793980
#define GPIODRV_H_ef314d0218d04f29abd02c15d2793980

#include <inttypes.h>

// LED red - RA3
#define LEDRED_TRISbits			TRISAbits
#define LEDRED_TRISPIN			TRISA3
#define LEDRED_LATbits			LATAbits
#define LEDRED_LATPIN			LATA3
#define LEDRED_LATINV			LATAINV
#define LEDRED_MASK				(1<<3)

// LED green - RB12
#define LEDGREEN_TRISbits		TRISBbits
#define LEDGREEN_TRISPIN		TRISB12
#define LEDGREEN_LATbits		LATBbits
#define LEDGREEN_LATPIN			LATB12
#define LEDGREEN_LATINV			LATBINV
#define LEDGREEN_MASK			(1<<12)
#define LEDGREEN_ANSELbits		ANSELBbits
#define LEDGREEN_ANSELPIN		ANSB12

// LED blue - RA2
#define LEDBLUE_TRISbits		TRISAbits
#define LEDBLUE_TRISPIN			TRISA2
#define LEDBLUE_LATbits			LATAbits
#define LEDBLUE_LATPIN			LATA2
#define LEDBLUE_LATINV			LATAINV
#define LEDBLUE_MASK			(1<<2)

// Button 1 - RB7
#define BTN1_TRISbits			TRISBbits
#define BTN1_TRISPIN			TRISB7
#define BTN1_PORTbits			PORTBbits
#define BTN1_PORTPIN			RB7
#define BTN1_PULLREGbits		CNPUBbits	// Call also pull down with CNPDx
#define BTN1_PULLBIT			CNPUB7

// Button 2 - RB13
#define BTN2_TRISbits			TRISBbits
#define BTN2_TRISPIN			TRISB13
#define BTN2_PORTbits			PORTBbits
#define BTN2_PORTPIN			RB13
#define BTN2_PULLREGbits		CNPUBbits	// Call also pull down with CNPDx
#define BTN2_PULLBIT			CNPUB13
#define BTN2_ANSELbits			ANSELBbits
#define BTN2_ANSELPIN			ANSB13



// UART - RB15_RX -> RP10 and RB14_TX -> RP9
#define UART_TX_TRISbits		TRISBbits
#define UART_TX_TRISPIN			TRISB14
#define UART_TX_LATbits			LATBbits
#define UART_TX_LATPIN			LATB14
#define UART_TX_MAP_REGbits		RPOR2bits
#define UART_TX_MAP_REGPERIPH	RP9R
#define UART_TX_MAP_REGVAL		1		// Maps to U2TX 
#define UART_TX_ANSELbits		ANSELBbits
#define UART_TX_ANSELPIN		ANSB14

#define UART_RX_TRISbits		TRISBbits
#define UART_RX_TRISPIN			TRISB15
#define UART_RX_PULLREGbits		CNPUBbits	// UARTs are idle high, so pull-up
#define UART_RX_PULLBIT			CNPUB15
#define UART_RX_MAP_REGbits		RPINR9bits
#define UART_RX_MAP_REGPERIPH	U2RXR
#define UART_RX_MAP_REGVAL		10		// Maps to RPxx pin
#define UART_RX_ANSELbits		ANSELBbits
#define UART_RX_ANSELPIN		ANSB15


void GPIODrv_Init();
void LEDred_setState(uint32_t state);
void LEDgreen_setState(uint32_t state);
void LEDblue_setState(uint32_t state);
uint32_t BTN1_GetState();
uint32_t BTN2_GetState();

#endif
