#include <p32xxxx.h>    // Always in first place to avoid conflict with const.h -> this includes xc.h
//#include <system.h>     // System setup
#include <const.h>      // MIPS32
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <newlib.h>
#include <errno.h>

#include <GPIODrv.h>
#include <UARTDrv.h>


// Configuration bits for the PIC32MM chip. PRIMARY configuration
const uint32_t __attribute__((section (".SECTION_FDEVOPT"))) temp_fdevopt = 
	0x0000FFF7
	| (0xF0C5 << _FDEVOPT_USERID_POSITION)	// UserID is F0C5.
												// Hence this hack, to have it aligned, since the definition shifts by 15, instead of 16...
	| (0b1 << _FDEVOPT_SOSCHP_POSITION);	// SOSC operated in Normal Power mode

const uint32_t __attribute__((section (".SECTION_FICD"))) temp_ficd =
	0xFFFFFFE3
	| (0b01 << _FICD_ICS_POSITION)			// Communication is on PGEC3/PGED3
	| (0b1 << _FICD_JTAGEN_POSITION);		// JTAG is enabled

const uint32_t __attribute__((section (".SECTION_FPOR"))) temp_fpor =
	0xFFFFFFF0
	| (0b0 << _FPOR_LPBOREN_POSITION)		// Low-Power BOR is disabled
	| (0b0 << _FPOR_RETVR_POSITION)			// Retention regulator is enabled, controlled by RETEN in sleep
	| (0b00 << _FPOR_BOREN_POSITION);		// Brown-out reset disabled in HW, SBOREN bit disabled

const uint32_t __attribute__((section (".SECTION_FWDT"))) temp_fwdt =
	0xFFFF0000
	| (0b0 << _FWDT_FWDTEN_POSITION)		// Watchdog Timer disabled
	| (0b00 << _FWDT_RCLKSEL_POSITION)		// Clock source is system clock
	| (0b10000 << _FWDT_RWDTPS_POSITION)	// Run mode Watchdog Postscale is 1:65536
	| (0b1 << _FWDT_WINDIS_POSITION)		// Windowed mode is disabled
	| (0b00 << _FWDT_FWDTWINSZ_POSITION)	// Watchdog Timer window size is 75%
	| (0b10000 << _FWDT_SWDTPS_POSITION);	// Sleep mode Watchdog Postscale is 1:65536 

const uint32_t __attribute__((section (".SECTION_FOSCSEL"))) temp_foscsel =
	0xFFFF2828
	| (0b00 << _FOSCSEL_FCKSM_POSITION)		// Clock switching disabled, Fail-Safe clock monitor disabled
	| (0b1 << _FOSCSEL_SOSCSEL_POSITION)	// Crystal is used for SOSC, RA4/RB4 controlled by SOSC
	| (0b1 << _FOSCSEL_OSCIOFNC_POSITION)	// System clock not on CLKO pin, operates as normal I/O
	| (0b11 << _FOSCSEL_POSCMOD_POSITION)	// Primary oscillator is disabled
	| (0b0 << _FOSCSEL_IESO_POSITION)		// Two-speed Start-up is disabled
	| (0b0 << _FOSCSEL_SOSCEN_POSITION)		// Secondary oscillator disabled
	| (0b1 << _FOSCSEL_PLLSRC_POSITION)		// FRC is input to PLL on device Reset
	| (0b001 << _FOSCSEL_FNOSC_POSITION);	// Oscillator is (Primary or FRC) with PLL. -> By default, it's set to x3 in SPLLCON, so 24MHz total. Fix / implement later.

const uint32_t __attribute__((section (".SECTION_FSEC"))) temp_fsec =
	0x7FFFFFFF
	| (0b1 << _FSEC_CP_POSITION);			// Code protection disabled
// End PRIMARY configuration

// Configuration bits for the PIC32MM chip. ALTERNATE configuration
const uint32_t __attribute__((section (".SECTION_AFDEVOPT"))) temp_afdevopt = 
	0x0000FFF7
	| (0xF0C5 << _FDEVOPT_USERID_POSITION)	// UserID is F0C5.
												// Hence this hack, to have it aligned, since the definition shifts by 15, instead of 16...
	| (0b1 << _FDEVOPT_SOSCHP_POSITION);	// SOSC operated in Normal Power mode

const uint32_t __attribute__((section (".SECTION_AFICD"))) temp_aficd =
	0xFFFFFFE3
	| (0b01 << _FICD_ICS_POSITION)			// Communication is on PGEC3/PGED3
	| (0b1 << _FICD_JTAGEN_POSITION);		// JTAG is enabled

const uint32_t __attribute__((section (".SECTION_AFPOR"))) temp_afpor =
	0xFFFFFFF0
	| (0b0 << _FPOR_LPBOREN_POSITION)		// Low-Power BOR is disabled
	| (0b0 << _FPOR_RETVR_POSITION)			// Retention regulator is enabled, controlled by RETEN in sleep
	| (0b00 << _FPOR_BOREN_POSITION);		// Brown-out reset disabled in HW, SBOREN bit disabled

const uint32_t __attribute__((section (".SECTION_AFWDT"))) temp_afwdt =
	0xFFFF0000
	| (0b0 << _FWDT_FWDTEN_POSITION)		// Watchdog Timer disabled
	| (0b00 << _FWDT_RCLKSEL_POSITION)		// Clock source is system clock
	| (0b10000 << _FWDT_RWDTPS_POSITION)	// Run mode Watchdog Postscale is 1:65536
	| (0b1 << _FWDT_WINDIS_POSITION)		// Windowed mode is disabled
	| (0b00 << _FWDT_FWDTWINSZ_POSITION)	// Watchdog Timer window size is 75%
	| (0b10000 << _FWDT_SWDTPS_POSITION);	// Sleep mode Watchdog Postscale is 1:65536 

const uint32_t __attribute__((section (".SECTION_AFOSCSEL"))) temp_afoscsel =
	0xFFFF2828
	| (0b00 << _FOSCSEL_FCKSM_POSITION)		// Clock switching disabled, Fail-Safe clock monitor disabled
	| (0b1 << _FOSCSEL_SOSCSEL_POSITION)	// Crystal is used for SOSC, RA4/RB4 controlled by SOSC
	| (0b1 << _FOSCSEL_OSCIOFNC_POSITION)	// System clock not on CLKO pin, operates as normal I/O
	| (0b11 << _FOSCSEL_POSCMOD_POSITION)	// Primary oscillator is disabled
	| (0b0 << _FOSCSEL_IESO_POSITION)		// Two-speed Start-up is disabled
	| (0b0 << _FOSCSEL_SOSCEN_POSITION)		// Secondary oscillator disabled
	| (0b1 << _FOSCSEL_PLLSRC_POSITION)		// FRC is input to PLL on device Reset
	| (0b001 << _FOSCSEL_FNOSC_POSITION);	// Oscillator is (Primary or FRC) with PLL. -> By default, it's set to x3 in SPLLCON, so 24MHz total. Fix / implement later.

const uint32_t __attribute__((section (".SECTION_AFSEC"))) temp_afsec =
	0x7FFFFFFF
	| (0b1 << _FSEC_CP_POSITION);			// Code protection disabled
// End ALTERNATE configuration

char tempArray[128];
uint8_t lengthArray = 0;
uint32_t counter = 0;
float FLOATtest = 0.1f;
uint32_t bytesRxdUart = 0;

// TODO - run the timer, like in MX440 example (SysTick style)
void simpleDelay(unsigned int noOfLoops){
    unsigned int i = 0;
    while (i<noOfLoops){
        i++;
        asm("nop");
    }
}

void setup(){
	// What is the equivalent of SYSTEMConfigPerformance?
	// -> Setting up the system for the required System Clock
	// -> Seting up the Wait States
	// -> Setting up PBCLK
	// -> Setting up Cache module (not presenf on MX1/2, but is on MX4)
	// Also of interest: https://microchipdeveloper.com/32bit:mx-arch-exceptions-processor-initialization
	// See Pic32 reference manual, for CP0 info http://ww1.microchip.com/downloads/en/devicedoc/61113e.pdf

	// DO NOT setup KSEG0 (cacheable are) on MX1/MX2, debugging will NOT work

	//BMXCONbits.BMXWSDRM = 0;	// Set wait-states to 0
	
	// System config, call with desired CPU freq. and PBCLK divisor
	//SystemConfig(48000000L, 1);	// Set to 48MHz, with PBCLK with divider 1 (same settings as DEVCFG)

	//UARTDrv_Init(115200);

	// Enable interrupts
	//INTEnableSystemMultiVectoredInt();
}

void MIPS32 INTEnableSystemMultiVectoredInt(void)
{
    uint32_t val;

    // set the CP0 cause IV bit high
    asm volatile("mfc0   %0,$13" : "=r"(val));
    val |= 0x00800000;
    asm volatile("mtc0   %0,$13" : "+r"(val));

    INTCONSET = _INTCON_MVEC_MASK;

    // set the CP0 status IE bit high to turn on interrupts
    //INTEnableInterrupts();
	asm("ei");
}


void _general_exception_handler(){
	asm("nop");
	asm("nop");
	asm("nop");
	for(;;);
}


int main(){


//	setup();

	GPIODrv_Init();
	UARTDrv_Init(115200);

	INTCONbits.MVEC=1; 
	INTEnableSystemMultiVectoredInt();

    for(;;){
		bytesRxdUart = UARTDrv_GetCount();

		lengthArray = snprintf(tempArray, sizeof(tempArray), "Hello from PIC32MM! Counter = %ld\n", counter++);
		UARTDrv_SendBlocking((uint8_t *)tempArray, lengthArray);

		lengthArray = snprintf(tempArray, sizeof(tempArray), "BTN1: %ld, BTN2: %ld, RXd bytes: %ld\n", BTN1_GetState(), BTN2_GetState(), bytesRxdUart);
		UARTDrv_SendBlocking((uint8_t *)tempArray, lengthArray);
		
		lengthArray = snprintf(tempArray, sizeof(tempArray), "Float test: %f\n", FLOATtest);
		UARTDrv_SendBlocking((uint8_t *)tempArray, lengthArray);
		

		if ((bytesRxdUart & 0x03) == 0){
			LEDred_setState(1);
			LEDgreen_setState(1);
			LEDblue_setState(1);
		}
		else if ((bytesRxdUart & 0x03) == 1){
			LEDred_setState(1);
			LEDgreen_setState(0);
			LEDblue_setState(0);
		}
		else if ((bytesRxdUart & 0x03) == 2){
			LEDred_setState(0);
			LEDgreen_setState(1);
			LEDblue_setState(0);
		}
		else if ((bytesRxdUart & 0x03) == 3){
			LEDred_setState(0);
			LEDgreen_setState(0);
			LEDblue_setState(1);
		}

		simpleDelay(2000000);

		FLOATtest = FLOATtest + 0.314f;
    }

/**********************************************************************/

    return(0);
    
} // end of main

