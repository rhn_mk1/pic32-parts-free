This is an example program for the ChipKit DP32 dev board.
The board contains a PIC32MX250F128B microcontroller, some LEDs, buttons, etc.
This example is meant to be uploaded with a programmer, and not through a bootloader.


LED2 is used as an LED, toggles on each loop iteration.
LED1 is shared with UART TX, blinks when transmitting.
LED3 and LED4 are shared with ICSP lines.

BTN2/PGM is used as an input.

UART1 is used to show UART transmitting (blocking), and UART receiving (interrupts)
PB3 = TX, PB13 = RX
(Connect your own USB-Serial converter, to see communication)


Things TODO:
- isrwrapper should be filled with all ISRs for MX250
-- Possibly have one isrwrapper per chip? Might be easier
- Sprintf with numbers fails (also consumes 45kB of code)
-- Malloc issue / calling malloc fails. Possibly newlib issue? Uncertain
- system.c was updated with some functionality. Seems to work, should check on other chips.
- Try debugging via JTAG
