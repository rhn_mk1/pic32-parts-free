#ifndef GPIODRV_H_573f2ca8b43e44209acc90f0f9b7de5d
#define GPIODRV_H_573f2ca8b43e44209acc90f0f9b7de5d

// LED
#define LEDUSER_TRISbits		TRISCbits
#define LEDUSER_TRISPIN			TRISC4
#define LEDUSER_LATbits			LATCbits
#define LEDUSER_LATPIN			LATC4
#define LEDUSER_LATINV			LATCINV
#define LEDUSER_MASK			(1<<4)

// Button
#define BTNUSER_TRISbits		TRISCbits
#define BTNUSER_TRISPIN			TRISC5
#define BTNUSER_PORTbits		PORTCbits
#define BTNUSER_PORTPIN			RC5
#define BTNUSER_PULLREG			CNPUC	// Call also pull down with CNPDx
#define BTNUSER_PULLBIT			(1<<5)

// UART
#define UART_TX_TRISbits		TRISCbits
#define UART_TX_TRISPIN			TRISC8
#define UART_TX_LATbits			LATCbits
#define UART_TX_LATPIN			LATC8
#define UART_TX_RP_REG			RPC9R
#define UART_TX_RP_VAL			0b0010	// U2TX

#define UART_RX_TRISbits		TRISCbits
#define UART_RX_TRISPIN			TRISC9
#define UART_RX_PULLREG			CNPUC	// UARTs are idle high, so pull-up
#define UART_RX_PULLBIT			(1<<9)
#define UART_RX_REMAP_VAL		0b0110

#endif
