MX440 Examples
==============

The examples in this section are designed for the [PIC32-PINGUINO-MICRO](https://gitlab.com/rhn/pic32-parts-free/-/wikis/boards/PIC32-pinguino-micro) board.

They all switch GPIO in order to blink in one way or the other.

- `blink_cpu` blinks a LED, delaying by counting instructions
- `blink_timer` delays by reading out timer register values
- `blink_intr` blinks without delays, by using timer interrupts

To run each, find the corresponding `build_*.sh` file, and run:

```
sh ./build_blink_cpu.sh mipsel
```

The resulting `.hex` file can be uploaded to the device by [flashing](https://gitlab.com/rhn/pic32-parts-free/-/wikis/Flashing).
