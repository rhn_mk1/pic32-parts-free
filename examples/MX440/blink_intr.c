/*  --------------------------------------------------------------------
    FILE:               main32.c
    PROJECT:            pinguino 32
    PURPOSE:            application main function
    PROGRAMERS:         Regis Blanchot <rblanchot@gmail.com>
                        Jean-Pierre Mandon <jp.mandon@gmail.com>
    FIRST RELEASE:      16 Nov. 2010
    LAST RELEASE:       20 Mar. 2015
    --------------------------------------------------------------------
    CHANGELOG:

    22 Sep. 2011    M.Fazzi added UART3,4,5,6 support
    14 Jan. 2015    R.Blanchot added OnTimerX support
    03 Mar. 2015    R.Blanchot moved interrupt weak definitions in isrwrapper.c
    20 Mar. 2015    R.Blanchot removed SystemConfig() (defined in the bootloader)
    14 Apr. 2015    R.Blanchot added SPI init.
    --------------------------------------------------------------------
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
----------------------------------------------------------------------*/

/**
 * This example blinks the red LED using interrupts in multi-interrupt mode.
 */

#include <p32xxxx.h>    // Always in first place to avoid conflict with const.h ON
#include <const.h>      // Pinguino's constants definitions
#include "define.h"     // Pinguino Sketch Constants
#include <io.c>         // Pinguino Boards Peripheral Remappage and IOs configurations
#include <delay.c>
#include <digitalw.c>
#include <tools.c>

#define USERLED                 32      //PORTGbits.RG6 // led1
#define TWOLED                 10      //PORTGbits.RG6 // led2

void _general_exception_handler() {}

void Timer1Interrupt() {
    toggle(TWOLED);
    IFS0CLR = 0x10;
}

void setup() {
    pinmode(USERLED, OUTPUT);
    pinmode(TWOLED, OUTPUT);

    INTCONSET = _INTCON_MVEC_MASK;

    asm("ei");
    T1CON = 0;
    TMR1 = 0;
    PR1 = 0xffff;
    IPC1 |= 0x4;
    IPC1 |= 0x1;
    IFS0 &= ~(0x10);
    IEC0 |= 0x10;
    T1CON = 0x30;
    T1CON |= 0x8000;
}

void loop() {
    if (TMR1 & 0x4000) {
        high(USERLED);
    } else {
        low(USERLED);
    }
}

int main()
{
    // Configure pins
    IOsetDigital();
    IOsetSpecial();
    #if defined(__SERIAL__) || defined(__SPI__) || \
        defined(__PWM__)    || defined(__AUDIO__)
    IOsetRemap();
    #endif

    // Different init.
    
    #ifdef __WATCHDOG__
    watchdog_init();
    #endif

    #ifdef __ANALOG__
    analog_init();
    #endif

    #ifdef __MILLIS__
    millis_init();
    #endif

    #ifdef __PWM__
    PWM_init();
    #endif    

    #ifdef __USBCDC
    CDC_init();
    #endif    

    //#ifdef __RTCC__
    //RTCC_init();
    //#endif    

    #ifdef __SPI__
    SPI_init();
    #endif    
    
    #ifdef __SERVOS__
    servos_init();
    #endif    

/** USER'S SKETCH *****************************************************/

    setup();

    while (1)
    {
        #ifdef __USBCDC
            #if defined(__32MX220F032D__) || \
                defined(__32MX220F032B__) || \
                defined(__32MX250F128B__) || \
                defined(__32MX270F256B__)
                USB_Service();
            #else
                CDCTxService();
            #endif
        #endif
 
        loop();
    }
/**********************************************************************/

    return(0);
    
} // end of main
