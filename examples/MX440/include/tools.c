#ifndef __TOOLS_C
#define __TOOLS_C

#include <inttypes.h>
#include <io.c>         // Pinguino Boards Peripheral Remappage and IOs configurations

#define USERLED                 32      //PORTGbits.RG6 // led1
#define TWOLED                 10      //PORTGbits.RG6 // led2

void blink_value(uint32_t value) {
    for (unsigned i = 0; i < 32; i++) {
        high(USERLED);
        if ((value >> i) & 1) {
            high(TWOLED);
        } else {
            low(TWOLED);
        }
        Delayms(500);
        low(USERLED);
        Delayms(500);
    }
}
#endif
