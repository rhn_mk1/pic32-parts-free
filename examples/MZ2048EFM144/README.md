PIC32MZ  example project
===========================

This is an example program for the Olimex [PIC32-MHZ144](https://www.olimex.com/Products/PIC/Development/PIC32-HMZ144/open-source-hardware) board, that has a [PIC32MZ2048EFM144](https://www.microchip.com/wwwproducts/en/PIC32MZ2048EFM144) on it.

## Pins used
The folowing pins are used or reserved by the example program:
- LED
    - RH2, active HIGH
- Button
    - RB12, active LOW, external pull-up
- UART @ 115200bps
    - U1TX -> RD11
    - U1RX -> RD10
- ICSP
    - RB0 -> PGED1
    - RB1 -> PGEC1
- JTAG pins on the separate header
    - TCK -> RA1
    - TMS -> RA0
    - TDI -> RF13
    - TDO -> RF12

## Program overview
The following things are tested with this example program:
- GPIO (LED writing, Button reading)
- UART (blocking TX at 115200bps, RX interrupt)
- Interrupts (see UART RX interrupt)
- sprintf test with integers and floats



The example blinks an LED slowly (1s total time) if the button isn't pressed, or quickly (0.4s total), if the button is pressed.
On each iteration, a message is printed, for example `Blinking LED quickly... turning ON. Counter = %ld\n`.

To test the UART RX interrupt, a counter is increased in the ISR, and printed in the main loop `RXd bytes: %ld\n`

To test floating point operations, a float is printed in the main loop `Float test: %f\n`
Note that for testing floating point number in sprintf, it is necessary to include the proper link option in the Makefile -> `-u _printf_float`. If you don't require floating point support in printing, leave that out, as the size difference is quite huge (cca. extra 30kB).

The whole output should look something like this

>Blinking LED slowly... turning ON. Counter = 0
Blinking LED slowly... turning OFF. Counter = 1
RXd bytes: 0
Float test: 0.100000
Blinking LED slowly... turning ON. Counter = 1
Blinking LED slowly... turning OFF. Counter = 2
RXd bytes: 0
Float test: 0.414000
Blinking LED slowly... turning ON. Counter = 2
Blinking LED slowly... turning OFF. Counter = 3
RXd bytes: 0
Float test: 0.728000
...
