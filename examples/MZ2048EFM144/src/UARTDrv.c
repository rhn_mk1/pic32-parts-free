#include <UARTDrv.h>
#include <GPIODrv.h>
#include <interrupt.h>

volatile uint32_t count = 0;
uint8_t tempDiscard;

// As defined in interrupt.h/c
INTERRUPT(UART1RxInterrupt){
	count++;		// Increase counter, for our test
	tempDiscard = U1RXREG;	// Readout data, otherwise we'll be stuck here
	IFS3CLR = 1<<17;	// Clear bits. TODO nicer. 
}


void UARTDrv_Init(uint32_t pbclk2, uint32_t baud){
	// Using UART1, with pins RD11 (TX) and RD10 (RX)
	U1MODEbits.ON = 0;

	UART_TX_TRISbits.UART_TX_TRISPIN = 0;	// 0 == output
	UART_TX_LATbits.UART_TX_LATPIN = 1;		// Set high, as UART is Idle High
	RPD11R = UART_TX_REMAP;					// Remap to proper pin

	UART_RX_TRISbits.UART_TX_TRISPIN = 1;	// 1 == input
	UART_RX_CNPUbits.UART_RX_CNPUBIT = 1;	// Enable pull-up
	U1RXR = UART_RX_REMAP;					// Remap to proper pin
	
	U1MODEbits.SIDL = 0;	// Stop when in IDLE mode
	U1MODEbits.IREN	= 0;	// Disable IrDA
	U1MODEbits.RTSMD = 0;	// Don't care, RTS not used
	U1MODEbits.UEN = 0;		// TX & RX controlled by UART peripheral, RTC & CTS are not.
	U1MODEbits.WAKE = 0;	// Don't wake up from sleep
	U1MODEbits.LPBACK = 0;	// Loopback mode disabled
	U1MODEbits.ABAUD = 0;	// No autobauding
	U1MODEbits.RXINV = 0;	// Idle HIGH
	U1MODEbits.BRGH = 0;	// Standard speed mode - 16x baud clock
	U1MODEbits.PDSEL = 0;	// 8 bits, no parity
	U1MODEbits.STSEL = 0;	// 1 stop bit

	U1STAbits.ADM_EN = 0;	// Don't care for auto address detection, unused
	U1STAbits.ADDR = 0;		// Don't care for auto address mark
	U1STAbits.UTXISEL = 0;	// Generate interrupt, when at least one space available (unused)
	U1STAbits.UTXINV = 0;	// Idle HIGH
	U1STAbits.URXEN = 1;	// UART receiver pin enabled
	U1STAbits.UTXBRK = 0;	// Don't send breaks.
	U1STAbits.UTXEN = 1;	// Uart transmitter pin enabled
	U1STAbits.URXISEL = 0;	// Interrupt what receiver buffer not empty
	U1STAbits.ADDEN = 0;	// Address detect mode disabled (unused)
	U1STAbits.OERR = 0;		// Clear RX Overrun bit - not important at this point

	//U1BRG = 12;			// Should calculate as (PBCLK2/BRGH?4:16)/BAUD - 1
	U1BRG = (pbclk2 / (U1MODEbits.BRGH ? 4 : 16)) / baud - 1;

	// New - setup interrupt
	IPC28bits.U1RXIP = 1;		// Priorty = 1 (one above disabled)
	IPC28bits.U1RXIS = 0;		// Subpriority = 0 (least)
	IEC3bits.U1RXIE = 1;	// Enable interrupt	

	U1MODEbits.ON = 1;
}

void UARTDrv_SendBlocking(uint8_t * buffer, uint32_t length){
	
	uint32_t counter = 0;

	for (counter = 0; counter<length; counter++){
		while(U1STAbits.UTXBF){ asm("nop"); }
		U1TXREG = buffer[counter];
	}

	// Wait until sent
	while(U1STAbits.TRMT){
		asm("nop");
	}
}

uint32_t UARTDrv_GetCount(){
	return count;
}


