#include <p32xxxx.h>
#include <inttypes.h>
#include <GPIODrv.h>
#include <BTNDrv.h>
#include <UARTDrv.h>

#include <string.h>
#include <stdio.h>
#include <newlib.h>
#include <errno.h>

// Interrupts
//#include <interrupt.h>

// Like, a bunch of config bits
// Note, this is experimental. This just sets bits in the Lower Boot alias,
// not in both Boot Flash 1 and 2. It works, though.

// ADEVCFG3
const uint32_t __attribute__((section (".SECTION_ADEVCFG3"))) ADEVCFG3_temp = 
	0x84FF0000
	| (0b0 << _ADEVCFG3_FUSBIDIO_POSITION)
	| (0b0 << _ADEVCFG3_IOL1WAY_POSITION)
	| (0b0 << _ADEVCFG3_PMDL1WAY_POSITION)
	| (0b0 << _ADEVCFG3_PGL1WAY_POSITION)
	| (0b0 << _ADEVCFG3_FETHIO_POSITION)
	| (0b0 << _ADEVCFG3_FMIIEN_POSITION)
	| (0xBEEF << _ADEVCFG3_USERID_POSITION);
	
// Input is a 24MHz crystal.
// To achieve 200MHz, it must be:
// - Divide by 3 == 8MHz (Limit 5-64MHz)
// - Multiply by 50 == 400MHz (Limit 350-700MHz)
// - Divide by 2 == 200MHz  (Limit 10-200MHz)
const uint32_t __attribute__((section (".SECTION_ADEVCFG2"))) ADEVCFG2_temp =
	0xBFF88008
	| (0b1 << _ADEVCFG2_UPLLFSEL_POSITION)		// UPLL input clock is 24MHz
	| (0b000 << _ADEVCFG2_FPLLODIV_POSITION)	// PLL Output divided by 2
	| (0b0110001 << _ADEVCFG2_FPLLMULT_POSITION)// PLL Multipler is 50 (-1 == 49)
	| (0b0 << _ADEVCFG2_FPLLICLK_POSITION)		// Posc is input to PLL
	| (0b010 << _ADEVCFG2_FPLLRNG_POSITION)		// Input to PLL is in 8-16MHz range
	| (0b010 << _ADEVCFG2_FPLLIDIV_POSITION);	// Divide input by 3
	
const uint32_t __attribute__((section (".SECTION_ADEVCFG1"))) ADEVCFG1_temp =
	0x00003800
	| (0b0 << _ADEVCFG1_FDMTEN_POSITION)		// Deadman timer disabled, can be enabled in SW
	| (0b00000 << _ADEVCFG1_DMTCNT_POSITION)	// Deadman counter set to 256
	| (0b00 << _ADEVCFG1_FWDTWINSZ_POSITION)	// Watchdog windows size is 75%
	| (0b0 << _ADEVCFG1_FWDTEN_POSITION)		// Watchdog timer not enabled, can be in SW
	| (0b0 << _ADEVCFG1_WINDIS_POSITION)		// Watchdog timer in Window mode
	| (0b1 << _ADEVCFG1_WDTSPGM_POSITION)		// Watchdog timer stops during Flash programming
	| (0b00000 << _ADEVCFG1_WDTPS_POSITION)		// Watchdog postscale is 1:1
	| (0b11 << _ADEVCFG1_FCKSM_POSITION)		// Clock swiching enabled, clock monitor enabled
	| (0b1 << _ADEVCFG1_OSCIOFNC_POSITION)		// CLKO output disabled
	| (0b10 << _ADEVCFG1_POSCMOD_POSITION)		// HS Oscillator mode selected
	| (0b1 << _ADEVCFG1_IESO_POSITION)			// Internal-External switchover enabled
	| (0b0 << _ADEVCFG1_FSOSCEN_POSITION)		// Sosc disabled
	| (0b000 << _ADEVCFG1_DMTINTV_POSITION)		// Deadman window/interval is zero
	| (0b001 << _ADEVCFG1_FNOSC_POSITION);		// SPLL (System PLL) Selected as oscilator
	
const uint32_t __attribute__((section (".SECTION_ADEVCFG0"))) ADEVCFG0_temp =
	0xBFC07880
	| (0b1 << _ADEVCFG0_EJTAGBEN_POSITION)		// Normal EJTAG functionality (Boot?)
	| (0b0 << _ADEVCFG0_POSCBOOST_POSITION)		// Normal start of the Posc oscillator
	| (0b01 << _ADEVCFG0_POSCGAIN_POSITION)		// Posc gain set to 1 (default is 2)
	| (0b0 << _ADEVCFG0_SOSCBOOST_POSITION)		// Normal start of the Sosc oscillator
	| (0b01 << _ADEVCFG0_SOSCGAIN_POSITION)		// Sosc gain set to 1 (default is 2)
	| (0b0 << _ADEVCFG0_SMCLR_POSITION)			// MCLR generates a POR Reset
	| (0b0 << _ADEVCFG0_FSLEEP_POSITION)		// FLash remains powered when device is in sleep
	| (0b11 << _ADEVCFG0_FECCCON_POSITION)		// ECC and dynamic ECC are disabled, ECCCON bits writable
	| (0b1 << _ADEVCFG0_BOOTISA_POSITION)		// Boot code and exceptions are in MIPS32
	| (0b0 << _ADEVCFG0_TRCEN_POSITION)			// Trace features disabled
	| (0b11 << _ADEVCFG0_ICESEL_POSITION)		// PGEC1/PGED1 pair used
	| (0b1 << _ADEVCFG0_JTAGEN_POSITION)		// JTAG is enabled
	| (0b11 << _ADEVCFG0_DEBUG_POSITION);		// Background debugger disabled >< - MUST be 11, 10 will NOT run
	
const uint32_t __attribute__((section (".SECTION_ADEVCP3"))) ADEVCP3_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_ADEVCP2"))) ADEVCP2_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_ADEVCP1"))) ADEVCP1_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_ADEVCP0"))) ADEVCP0_temp =	
	0xEFFFFFFF
	| (0b1 << _ADEVCP0_CP_POSITION);			// Protection is disabled

const uint32_t __attribute__((section (".SECTION_ADEVSIGN3"))) ADEVSIGN3_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_ADEVSIGN2"))) ADEVSIGN2_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_ADEVSIGN1"))) ADEVSIGN1_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_ADEVSIGN0"))) ADEVSIGN0_temp =	0x7FFFFFFF;	// There's always _one_ bit

// DEVCFG3
const uint32_t __attribute__((section (".SECTION_DEVCFG3"))) DEVCFG3_temp = 
	0x84FF0000
	| (0b0 << _ADEVCFG3_FUSBIDIO_POSITION)
	| (0b0 << _ADEVCFG3_IOL1WAY_POSITION)
	| (0b0 << _ADEVCFG3_PMDL1WAY_POSITION)
	| (0b0 << _ADEVCFG3_PGL1WAY_POSITION)
	| (0b0 << _ADEVCFG3_FETHIO_POSITION)
	| (0b0 << _ADEVCFG3_FMIIEN_POSITION)
	| (0xBEEF << _ADEVCFG3_USERID_POSITION);
	
const uint32_t __attribute__((section (".SECTION_DEVCFG2"))) DEVCFG2_temp =
	0xBFF88008
	| (0b1 << _DEVCFG2_UPLLFSEL_POSITION)		// UPLL input clock is 24MHz
	| (0b000 << _DEVCFG2_FPLLODIV_POSITION)		// PLL Output divided by 2
	| (0b0110001 << _DEVCFG2_FPLLMULT_POSITION)	// PLL Multipler is 50 (-1 == 49)
	| (0b0 << _DEVCFG2_FPLLICLK_POSITION)		// Posc is input to PLL
	| (0b010 << _DEVCFG2_FPLLRNG_POSITION)		// Input to PLL is in 8-16MHz range
	| (0b010 << _DEVCFG2_FPLLIDIV_POSITION);	// Divide input by 3
const uint32_t __attribute__((section (".SECTION_DEVCFG1"))) DEVCFG1_temp = 
	0x00003800
	| (0b0 << _ADEVCFG1_FDMTEN_POSITION)		// Deadman timer disabled, can be enabled in SW
	| (0b00000 << _ADEVCFG1_DMTCNT_POSITION)	// Deadman counter set to 256
	| (0b00 << _ADEVCFG1_FWDTWINSZ_POSITION)	// Watchdog windows size is 75%
	| (0b0 << _ADEVCFG1_FWDTEN_POSITION)		// Watchdog timer not enabled, can be in SW
	| (0b0 << _ADEVCFG1_WINDIS_POSITION)		// Watchdog timer in Window mode
	| (0b1 << _ADEVCFG1_WDTSPGM_POSITION)		// Watchdog timer stops during Flash programming
	| (0b00000 << _ADEVCFG1_WDTPS_POSITION)		// Watchdog postscale is 1:1
	| (0b11 << _ADEVCFG1_FCKSM_POSITION)		// Clock swiching enabled, clock monitor enabled
	| (0b1 << _ADEVCFG1_OSCIOFNC_POSITION)		// CLKO output disabled
	| (0b10 << _ADEVCFG1_POSCMOD_POSITION)		// HS Oscillator mode selected
	| (0b1 << _ADEVCFG1_IESO_POSITION)			// Internal-External switchover enabled
	| (0b0 << _ADEVCFG1_FSOSCEN_POSITION)		// Sosc disabled
	| (0b000 << _ADEVCFG1_DMTINTV_POSITION)		// Deadman window/interval is zero
	| (0b001 << _ADEVCFG1_FNOSC_POSITION);		// SPLL (System PLL) Selected as oscilator
	
const uint32_t __attribute__((section (".SECTION_DEVCFG0"))) DEVCFG0_temp =
	0xBFC07880
	| (0b1 << _ADEVCFG0_EJTAGBEN_POSITION)		// Normal EJTAG functionality (Boot?)
	| (0b0 << _ADEVCFG0_POSCBOOST_POSITION)		// Normal start of the Posc oscillator
	| (0b01 << _ADEVCFG0_POSCGAIN_POSITION)		// Posc gain set to 1 (default is 2)
	| (0b0 << _ADEVCFG0_SOSCBOOST_POSITION)		// Normal start of the Sosc oscillator
	| (0b01 << _ADEVCFG0_SOSCGAIN_POSITION)		// Sosc gain set to 1 (default is 2)
	| (0b0 << _ADEVCFG0_SMCLR_POSITION)			// MCLR generates a POR Reset
	| (0b0 << _ADEVCFG0_FSLEEP_POSITION)		// FLash remains powered when device is in sleep
	| (0b11 << _ADEVCFG0_FECCCON_POSITION)		// ECC and dynamic ECC are disabled, ECCCON bits writable
	| (0b1 << _ADEVCFG0_BOOTISA_POSITION)		// Boot code and exceptions are in MIPS32
	| (0b0 << _ADEVCFG0_TRCEN_POSITION)			// Trace features disabled
	| (0b11 << _ADEVCFG0_ICESEL_POSITION)		// PGEC1/PGED1 pair used
	| (0b1 << _ADEVCFG0_JTAGEN_POSITION)		// JTAG is enabled
	| (0b11 << _ADEVCFG0_DEBUG_POSITION);		// Background debugger disabled >< - MUST be 11, 10 will NOT run
const uint32_t __attribute__((section (".SECTION_DEVCP3"))) DEVCP3_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_DEVCP2"))) DEVCP2_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_DEVCP1"))) DEVCP1_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_DEVCP0"))) DEVCP0_temp =	ADEVCP0_temp;
const uint32_t __attribute__((section (".SECTION_DEVSIGN3"))) DEVSIGN3_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_DEVSIGN2"))) DEVSIGN2_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_DEVSIGN1"))) DEVSIGN1_temp =	0xFFFFFFFF;
const uint32_t __attribute__((section (".SECTION_DEVSIGN0"))) DEVSIGN0_temp =	0x7FFFFFFF;	// There's always _one_ bit
	


uint8_t sprintfTest[128];
uint32_t length = 0;

float LEDfloat = 0.1f;   
	
void simpleDelay(int32_t delay){

	while(delay--){
		asm("nop");
	}

}

int main(){

	// There is a single LED on RH2, achive High
	// Not an analog input, so no ANSEL
	LED_TRISbits.LED_TRISPIN = 0;	// Set LED pin to output
	LED_LATbits.LED_LATPIN = 1;		// Turn LED on
        
    // Setup button
    BTNDrv_Init();
       
    // Setup uart
    UARTDrv_Init(100000000, 115200);
    
    // Enable interrupts.
    
    INTCON = (1<<12);	// Set MVEC bit for multi vector interrupts
    // PRISS can be set to default, we don't use shadow registers for now
    // OFFx can be set to default, we don't have offsets for vectors right now
    
    // Enable interrupts, by setting the CP0 status register IE bit to high. Global enable
	asm("ei");
    
    uint32_t LEDcounter = 0;

    
    for(;;){
    	// If button pressed, blink faster
    	if (BTNDrv_GetButtonState()){
    		length = sprintf((char *)sprintfTest, "Blinking LED quickly... turning ON. Counter = %ld\n", LEDcounter++); 
		    UARTDrv_SendBlocking(sprintfTest, length);
    		LED_LATbits.LED_LATPIN = 1;
			simpleDelay(10000000);	// ~ ?s
		    
		    length = sprintf((char *)sprintfTest, "Blinking LED quickly... turning OFF. Counter = %ld\n", LEDcounter); 
		    UARTDrv_SendBlocking(sprintfTest, length);
		    LED_LATbits.LED_LATPIN = 0;
		    simpleDelay(10000000);	// ~ 1/10s   

    	}
    	else{
    		length = sprintf((char *)sprintfTest, "Blinking LED slowly... turning ON. Counter = %ld\n", LEDcounter++); 
		    UARTDrv_SendBlocking(sprintfTest, length);
		    LED_LATbits.LED_LATPIN = 1;
		    simpleDelay(25000000);	// ~ 1/4s
		    
		    length = sprintf((char *)sprintfTest, "Blinking LED slowly... turning OFF. Counter = %ld\n", LEDcounter); 
		    UARTDrv_SendBlocking(sprintfTest, length);
		    
		    LED_LATbits.LED_LATPIN = 0;
		    simpleDelay(25000000);	// ~ 1/4s

        }   
        
        length = sprintf((char *)sprintfTest, "RXd bytes: %ld\n", UARTDrv_GetCount()); 
	    UARTDrv_SendBlocking(sprintfTest, length);
	    
	    // The float test requires "-u _printf_float" passed to the linker!
	    // The build won't complain if it's missing, but the program will crash spectacularly
	    length = sprintf((char *)sprintfTest, "Float test: %f\n", LEDfloat); 
	    UARTDrv_SendBlocking(sprintfTest, length);
	            
        LEDfloat = LEDfloat + 0.314f;
    }

    return(0);
    
}



